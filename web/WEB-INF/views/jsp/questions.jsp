<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: atolstov
  Date: 12/2/17
  Time: 12:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Вопросы</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Where's my stuff?</a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span>Sign in</a></li>
            </ul>
        </div>
    </nav>
    <form:form>
        <table>

            <c:forEach items="${questionsList}" var="question">
                <tr>
                    <td><c:out value="${question.id}" /></td>
                    <td><c:out value="${question.dtcreated}" /></td>
                    <td><c:out value="${question.question}" /></td>
                    <td><c:out value="${question.user.username}" /></td>
                </tr>
            </c:forEach>
        </table>
    </form:form>
</body>
</html>
