CREATE TABLE users (
  iid   INTEGER IDENTITY PRIMARY KEY,
  sname VARCHAR(60),
  susername VARCHAR(30) NOT NULL,
  spassword VARCHAR(30) NOT NULL,
  savatarurl VARCHAR(150) DEFAULT 'http://cdn.schd.ws/common/img/avatar-empty.png'
);
----------------------------------------
CREATE TABLE questions (
  iid   INTEGER IDENTITY PRIMARY KEY,
  dtcreated TIMESTAMP,
  squestion VARCHAR(200),
  iuserid  INTEGER
);

ALTER TABLE questions ADD CONSTRAINT fk_questions_users FOREIGN KEY (iuserid) REFERENCES users(iid);

CREATE INDEX ix_questions_dtcreated ON questions(dtcreated);

-----------------------------------------
CREATE TABLE answers (
  iid   INTEGER IDENTITY PRIMARY KEY,
  dtcreated TIMESTAMP,
  stext VARCHAR(2000),
  iuserid  INTEGER,
  iquestionid  INTEGER
);

ALTER TABLE answers ADD CONSTRAINT fk_answers_users FOREIGN KEY (iuserid) REFERENCES users(iid);
ALTER TABLE answers ADD CONSTRAINT fk_answers_questions FOREIGN KEY (iquestionid) REFERENCES questions(iid);

CREATE INDEX ix_answers_dtcreated ON answers(dtcreated);
------------------------------------------
CREATE TABLE categories (
  iid   INTEGER IDENTITY PRIMARY KEY,
  sname VARCHAR(30)
);

CREATE TABLE questions_categories (
  iid   INTEGER IDENTITY PRIMARY KEY,
  iquestionid INTEGER,
  icategoryid INTEGER
);

ALTER TABLE questions_categories ADD CONSTRAINT fk_qc_questions FOREIGN KEY (iquestionid) REFERENCES questions(iid);
ALTER TABLE questions_categories ADD CONSTRAINT fk_qc_categories FOREIGN KEY (icategoryid) REFERENCES categories(iid);

