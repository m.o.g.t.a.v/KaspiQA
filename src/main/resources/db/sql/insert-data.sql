------------------------users  insert-------------------------------------
INSERT INTO users VALUES (1, 'Василий Петров', 'ashkhan', '123', 'http://cdn.schd.ws/common/img/avatar-empty.png');
INSERT INTO users VALUES (2, 'Иннокентий Щеглов', 'nordaik', '123', 'http://cdn.schd.ws/common/img/avatar-empty.png');
INSERT INTO users VALUES (3, 'Данила Петров', 'dpetrov', '123', 'http://cdn.schd.ws/common/img/avatar-empty.png');

------------------------questions  insert-------------------------------------
INSERT INTO questions VALUES (1, '2017-11-03 13:00:00', 'Почему небо голубое? Ответьте, пожалуйста. Вася, 35 годиков', 1);
INSERT INTO questions VALUES (2, '2017-06-01 01:00:00', 'Почему пельмени с бочки деда вкуснее, чем с ведра бабушки? Это долгая история....', 2);
INSERT INTO questions VALUES (3, '2017-12-03 15:45:00', 'Я потерял ключи прямо перед выходом. Где они? Я знаю, это вы их взяли', 3);

------------------------answers  insert-------------------------------------
INSERT INTO answers VALUES (1, '2017-11-03 13:30:00', 'Почитай книгу уже наконец', 3, 1);
INSERT INTO answers VALUES (2, '2017-06-01 05:00:00', 'Ты предвзят', 3, 2);
INSERT INTO answers VALUES (3, '2017-12-03 17:00:00', 'Уже вызываю санитаров', 1, 3);

--------------------------------categories insert-----------------------------
INSERT INTO categories VALUES (1, 'посуда');
INSERT INTO categories VALUES (2, 'техника');
INSERT INTO categories VALUES (3, 'одежда');
INSERT INTO categories VALUES (4, 'космос');
INSERT INTO categories VALUES (5, 'зс');

------------------------categories  insert-------------------------------------
INSERT INTO questions_categories VALUES (1, 1, 4);
INSERT INTO questions_categories VALUES (2, 2, 1);
INSERT INTO questions_categories VALUES (3, 2, 2);
INSERT INTO questions_categories VALUES (4, 2, 5);
INSERT INTO questions_categories VALUES (5, 3, 2);
INSERT INTO questions_categories VALUES (6, 3, 5);
