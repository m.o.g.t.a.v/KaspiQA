package main.java.JDBCtemplate;

import main.java.DAOinterface.QuestionDao;
import main.java.DAOinterface.UserDao;
import main.java.model.Question;
import main.java.model.QuestionMapper;
import main.java.model.User;
import main.java.model.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@SuppressWarnings("unchecked")
public class QuestionJDBCTemplate implements QuestionDao {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public void create(Timestamp dtcreated, String question, Integer userId) {
        String SQL = "INSERT INTO QUESTIONS (dtcreated, squestion, iuserid) VALUES (:dtcreated, :question, :userid)";
        Map namedParameters = new HashMap();
        namedParameters.put("dtcreated", new Timestamp(System.currentTimeMillis()));
        namedParameters.put("question", question);
        namedParameters.put("userid", userId);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    public Question getQuestion(Integer id) {
        String SQL = "SELECT * FROM QUESTIONS WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        return namedParameterJdbcTemplate.queryForObject(SQL, namedParameters, new QuestionMapper(new UserMapper()));
    }

    @Override
    public List<Question> listQuestions() {
        String SQL = "SELECT * FROM QUESTIONS ques" +
                     " INNER JOIN USERS us ON ques.iuserid = us.iid ";
        return namedParameterJdbcTemplate.query(SQL, new QuestionMapper(new UserMapper()));
    }

    @Override
    public List<Question> listQuestionsByUser(Integer userId) {
        String SQL = "SELECT * FROM QUESTIONS WHERE iuserid = :userid";
        Map namedParameters = new HashMap();
        namedParameters.put("userId", userId);
        return namedParameterJdbcTemplate.query(SQL, namedParameters, new QuestionMapper(new UserMapper()));
    }


    public void delete(Integer id) {
        String SQL = "DELETE FROM QUESTIONS WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }
    public void update(Integer id, String avatarUrl){
        String SQL = "UPDATE USERS SET sname = :name, savatarurl = :avatarurl WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        namedParameters.put("avatarurl", avatarUrl);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }
}