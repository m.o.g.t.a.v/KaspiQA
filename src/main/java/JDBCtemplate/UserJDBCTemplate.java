package main.java.JDBCtemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import main.java.DAOinterface.UserDao;
import main.java.model.User;
import main.java.model.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("unchecked")
public class UserJDBCTemplate implements UserDao {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public void create(String name, String username, String password, String avatarUrl) {
        String SQL = "INSERT INTO USERS (sname, susername, spassword, savatarurl) VALUES (:name, :username, :password, :avatarurl)";
        Map namedParameters = new HashMap();
        namedParameters.put("name", name);
        namedParameters.put("username", username);
        namedParameters.put("password", passwordEncoder.encode(password));
        namedParameters.put("avatarurl", avatarUrl);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }

    public User getUser(Integer id) {
        String SQL = "SELECT * FROM USERS WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        return namedParameterJdbcTemplate.queryForObject(SQL, namedParameters, new UserMapper());
    }

    @Override
    public List<User> listUsers() {
        String SQL = "SELECT * FROM USERS";
        return namedParameterJdbcTemplate.query(SQL, new UserMapper());
    }

    public void delete(Integer id) {
        String SQL = "DELETE FROM USERS WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }
    public void update(Integer id, String avatarUrl){
        String SQL = "UPDATE USERS SET sname = :name, savatarurl = :avatarurl WHERE iid = :id";
        Map namedParameters = new HashMap();
        namedParameters.put("id", id);
        namedParameters.put("avatarurl", avatarUrl);
        namedParameterJdbcTemplate.update(SQL, namedParameters);
    }
}