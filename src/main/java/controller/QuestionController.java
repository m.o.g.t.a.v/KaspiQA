package main.java.controller;

import main.java.DAOinterface.QuestionDao;
import main.java.DAOinterface.UserDao;
import main.java.model.Question;
import main.java.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class QuestionController {

    @Autowired
    QuestionDao questionDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showQuestions(Model model) {
        List<Question> questions = questionDao.listQuestions();
        model.addAttribute("questionsList", questions);
        return "questions";
    }
}
