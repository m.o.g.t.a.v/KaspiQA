package main.java.DAOinterface;

import main.java.model.User;

import java.util.List;
import javax.sql.DataSource;

public interface UserDao {

    public void create(String name, String username, String password, String avatarUrl);

    public User getUser(Integer id);

    public List<User> listUsers();

    public void delete(Integer id);

    public void update(Integer id, String avatarUrl);
}