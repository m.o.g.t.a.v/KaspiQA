package main.java.DAOinterface;

import main.java.model.Question;
import main.java.model.User;

import java.sql.Timestamp;
import java.util.List;

public interface QuestionDao {

    public void create(Timestamp dtcreated, String question, Integer userId);

    public Question getQuestion(Integer id);

    public List<Question> listQuestions();

    public List<Question> listQuestionsByUser(Integer userId);

    public void delete(Integer id);

    public void update(Integer id, String question);
}