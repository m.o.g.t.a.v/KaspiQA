package main.java.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("USERS.iid"));
        user.setName(rs.getString("USERS.sname"));
        user.setUsername(rs.getString("USERS.susername"));
        user.setAvatarUrl(rs.getString("USERS.savatarurl"));

        return user;
    }
}