package main.java.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class QuestionMapper implements RowMapper<Question> {

    private final UserMapper userMapper;

    public QuestionMapper(UserMapper userMapper){
         this.userMapper = userMapper;
    }

    public Question mapRow(ResultSet rs, int rowNum) throws SQLException {
        Question question = new Question();
        question.setId(rs.getInt("QUESTIONS.iid"));
        question.setDtcreated(rs.getTimestamp("QUESTIONS.dtcreated"));
        question.setQuestion(rs.getString("QUESTIONS.squestion"));
        question.setUserId(rs.getInt("QUESTIONS.iuserid"));
        question.setUser(userMapper.mapRow(rs, rowNum));
        return question;
    }
}