package main.java.model;

import main.java.DAOinterface.UserDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;

public class Question {

    @Autowired
    UserDao userDao;

    private int id;
    private Timestamp dtcreated;
    private String question;
    private int userId;
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDtcreated() {
        return dtcreated;
    }

    public void setDtcreated(Timestamp dtcreated) {
        this.dtcreated = dtcreated;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String squestion) {
        this.question = squestion;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    @Override
    public String toString() {
        return "Question{" + "id=" + id + ", dtcreated='" + dtcreated +
                ", question='" + question + ", userId=" + userId + '}';
    }
}
